//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by M.Usman Bin Rehan on 5/3/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- SIGN IN
    func signIn(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:"", params: params)! as URL
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- LOGOUT
    func logout(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route = POSTURLforRoute(route: WebRoute.Logout.rawValue)
        self.postRequestWith(route: route!, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
