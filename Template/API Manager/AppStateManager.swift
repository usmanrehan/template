//
//  AppStateManager.swift
//  The Court Lawyer
//
//  Created by M.Usman Bin Rehan on 5/3/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!
    
    override init() {
        super.init()
        if(!(realm != nil)){
            realm = try! Realm()
        }
        loggedInUser = realm.objects(User.self).first
    }
    
    func isUserLoggedIn() -> Bool{
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    
    func createUser(with userModel: User){
        self.loggedInUser = userModel
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add(self.loggedInUser, update: true)
        }
//        print(AppStateManager.sharedInstance.loggedInUser)
    }
    
    func logoutUser() {
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.delete(self.loggedInUser)
            self.loggedInUser = nil
        }
        AppDelegate.shared.changeRootViewController()
    }
}
