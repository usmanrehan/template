//
//  UIViewController+Extension.swift
//  Auditix
//
//  Created by M.Usman Bin Rehan on 1/3/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {

        //if your storyboard name is same as ControllerName uncomment it
        return "\(self)"
        
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
