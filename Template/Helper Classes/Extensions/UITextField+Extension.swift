//
//  UITextField+Extension.swift
//  Auditix
//
//  Created by M.Usman Bin Rehan on 1/3/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}
