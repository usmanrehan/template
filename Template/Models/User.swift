//
//  User.swift
//
//  Created by Hamza Hasan on 06/07/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let user_name = "email"
        static let password = "password"
        static let userId = "user_id"
    }
    
    // MARK: Properties
    @objc dynamic var user_name: String? = ""
    @objc dynamic var password: String? = ""
    @objc dynamic var userId: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "userId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        user_name <- map[SerializationKeys.user_name]
        password <- map[SerializationKeys.password]
        userId <- map[SerializationKeys.userId]
    }
    
    
}
